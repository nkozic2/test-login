import { render, cleanup, fireEvent, getByTestId } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect"
import Navbar from "./Navbar";
import Cookies from 'js-cookie'

afterEach(cleanup);

it("Navbar renders corectly", () => {
    render(<Navbar initiateLogout={() => {}} />);
})

it("Navbar logout event works", () => {
        const { container } = render(<Navbar initiateLogout={() => { Cookies.remove("token"); }}/>);
        const button = getByTestId(container, 'nav-button');
        expect(fireEvent.click(button)).toBe(true);
})