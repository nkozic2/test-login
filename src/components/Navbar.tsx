import { UserContext } from "../contexts/userContext";

interface INavbar {
  initiateLogout: () => void;
}

function Navbar({ initiateLogout }: INavbar) {
  return (
    <UserContext.Consumer>
      {(user) => (
        <nav
          data-testid="nav-test"
          className="navbar navbar-dark bg-dark px-3 "
        >
          <span className="navbar-brand">Dashboard</span>
          <div>
            <span className="text-light me-3">
              {user.firstName} {user.lastName}
            </span>
            <button
              data-testid="nav-button"
              className="btn btn-outline-success my-2 my-sm-0"
              type="submit"
              onClick={initiateLogout}
            >
              Log out
            </button>
          </div>
        </nav>
      )}
    </UserContext.Consumer>
  );
}
export default Navbar;
