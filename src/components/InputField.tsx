import { useField, ErrorMessage } from "formik";

export default function InputField({
  label,
  ...props
}: {
  label: string;
  type: string;
  name: string;
}) {
  const [filed, meta] = useField(props);
  return (
    <div className="mb-3">
      <label htmlFor={filed.name}>{label}</label>
      <input
        className={`form-control shadow-none max-width-input ${
          meta.touched && meta.error && "is-invalid"
        }`}
        maxLength={15}
        {...filed}
        {...props}
        autoComplete="off"
      />
      <ErrorMessage component="div" name={filed.name} className="error" />
    </div>
  );
}
