import { useState, useEffect } from "react";
import { axiosInstance } from "../api/axios";
import { AxiosError, AxiosResponse } from "axios";
import { IUser } from "../contexts/userContext";

export default function useGetUser() {
  const [user, setUser] = useState<IUser>({
    username: "",
    firstName: "",
    lastName: "",
  });

  const getUser = () => {
    axiosInstance
      .get("user")
      .then((response: AxiosResponse) => {
        const { username, firstName, lastName } = response.data;
        setUser({
          username: username,
          firstName: firstName,
          lastName: lastName,
        });
      })
      .catch((error: AxiosError) => {
        console.log("get user error", error);
      });
  };

  useEffect(() => {
    getUser();
  }, []);

  return user;
}
