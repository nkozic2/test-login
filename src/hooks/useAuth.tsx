import { useState } from "react";
import Cookies from "js-cookie";
import { AxiosResponse, AxiosError } from "axios";
import { axiosInstance } from "../api/axios";
import { useNavigate } from "react-router-dom";

export interface ICredentials {
  username: string;
  password: string;
}

export function useAuth() {
  const navigation = useNavigate();

  const [loginStatus, setLoginStatus] = useState("");

  const initiateLogin = (credentials: ICredentials) => {
    const { username, password } = credentials;
    axiosInstance
      .post("login", {
        username: username,
        password: password,
      })
      .then((response: AxiosResponse) => {
        const { token } = response.data;
        Cookies.set("token", token);
        navigation("/dashboard");
      })
      .catch((error: AxiosError) => {
        setLoginStatus(error.response?.data);
      });
  };

  const initiateLogout = () => {
    Cookies.remove("token");
    navigation("/login");
    setLoginStatus("");
  };

  return { initiateLogin, initiateLogout, loginStatus };
}
