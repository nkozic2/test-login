import { Navigate, Route, Routes } from "react-router-dom";
import Dashboard from "./Dashboard";
import LoginForm from "./LoginForm";
import Cookies from "js-cookie";
import { useAuth } from "../hooks/useAuth";

export default function PrivateRoutes() {
  const authenticated = Cookies.get("token");

  const { initiateLogin, initiateLogout, loginStatus } = useAuth();

  return (
    <Routes>
      {authenticated ? (
        <>
          <Route path="*" element={<Navigate replace to={"/dashboard"} />} />
          <Route
            path="/dashboard"
            element={<Dashboard initiateLogout={initiateLogout} />}
          />
        </>
      ) : (
        <>
          <Route path="*" element={<Navigate replace to={"/login"} />} />
          <Route
            path="/login"
            element={
              <LoginForm
                initiateLogin={initiateLogin}
                loginStatus={loginStatus}
              />
            }
          />
        </>
      )}
    </Routes>
  );
}
