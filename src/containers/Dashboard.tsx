import Navbar from "../components/Navbar";
import useGetUser from "../hooks/useGetUser";
import { UserProvider } from "../contexts/userContext";

interface IDashboard {
  initiateLogout: () => void;
}

function Dashboard({ initiateLogout }: IDashboard) {
  const user = useGetUser();
  return (
    <UserProvider value={user}>
      <Navbar initiateLogout={initiateLogout} />
    </UserProvider>
  );
}
export default Dashboard;
