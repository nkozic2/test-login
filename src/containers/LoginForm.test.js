import { render, fireEvent, getByTestId, cleanup } from "@testing-library/react";
import { act } from 'react-dom/test-utils';
import "@testing-library/jest-dom/extend-expect";
import LoginForm from "./LoginForm";

afterEach(cleanup);

it("LoginForm renders corectly", () => {
    render(<LoginForm initiateLogin={() => {}} loginStatus=""/>);
});

it("LoginForm login event works", async () => {
        await act(async () => {
            const { container } = render(<LoginForm initiateLogin={() => { }} loginStatus=""/>);
            const button = getByTestId(container, 'login-button');
            expect(fireEvent.click(button)).toBe(true);
        });
});

it("LoginForm label has good content", async () => {
    await act(async () => {
        const { container } = render(<LoginForm initiateLogin={() => { }} loginStatus="mock text"/>);
        expect(getByTestId(container, 'login-status')).toBeTruthy();
        expect(getByTestId(container, 'login-status')).toHaveTextContent('mock text');
    });
})