import { Formik, Form } from "formik";
import InputField from "../components/InputField";
import * as Yup from "yup";
import Logo from "../assets/logo.jpg";
import { ICredentials } from "../hooks/useAuth";

interface ILoginForm {
  initiateLogin: (credentials: ICredentials) => void;
  loginStatus: string;
}

export default function LoginForm({ initiateLogin, loginStatus }: ILoginForm) {
  const validate = Yup.object({
    username: Yup.string()
      .max(15, "Username can have maximum 15 characters.")
      .required("Username is required."),
    password: Yup.string()
      .min(5, "Password requires a minimum of 5 characters.")
      .max(15, "Password cant be longer then 15 characters.")
      .required("Password is required."),
  });

  return (
    <div className="gutter-background w-100 h-100">
      <div className="container h-100">
        <div className="row h-100 d-flex flex-column align-items-center">
          <div className="d-flex justify-content-center p-4">
            <img src={Logo} className="rounded img-thumbnail logo" alt="logo" />
          </div>
          <Formik
            initialValues={{
              username: "",
              password: "",
            }}
            onSubmit={(values) => initiateLogin(values)}
            validationSchema={validate}
          >
            {() => (
              <div className="pb-3 px-5 form-control d-inline-block max-width-form">
                <h1 className="my-4 d-inline-block font-weight-bold-display-4">
                  Login
                </h1>
                <Form>
                  <InputField label="Username" name="username" type="text" />
                  <InputField
                    label="Password"
                    name="password"
                    type="password"
                  />
                  <button
                    data-testid="login-button"
                    className="btn btn-dark mt-3"
                    type="submit"
                  >
                    Login
                  </button>
                </Form>
                {loginStatus && (
                  <label
                    data-testid="login-status"
                    className="lead mt-2 text-danger"
                  >
                    {loginStatus}
                  </label>
                )}
              </div>
            )}
          </Formik>
        </div>
      </div>
    </div>
  );
}
