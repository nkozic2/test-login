import PrivateRoutes from "./containers/PrivateRoutes";
import "./App.css";

export default function App() {
  return (
    <div className="App">
      <PrivateRoutes />
    </div>
  );
}
