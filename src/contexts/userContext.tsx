import { createContext } from "react";

export interface IUser {
  username: string;
  firstName: string;
  lastName: string;
}
const userContext = createContext({
  username: "",
  firstName: "",
  lastName: "",
});

export const UserProvider = userContext.Provider;
export const UserContext = userContext;
