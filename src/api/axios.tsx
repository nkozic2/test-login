import Axios, { AxiosRequestConfig, AxiosError } from "axios";
import Cookies from "js-cookie";

const axiosInstance = Axios.create({
  baseURL: process.env.REACT_APP_API_ROOT_PATH,
});

axiosInstance.interceptors.request.use(
  (config: AxiosRequestConfig) => {
    const token = Cookies.get("token");

    if (token) config.headers!["Authorization"] = token;

    return config;
  },
  (error: AxiosError) => {
    Promise.reject(error);
  }
);
export { axiosInstance };
